import java.util.LinkedList;

public class BlockAggregator {

  public static LinkedList<Range> aggregate(LinkedList<Long> blockOffsets,
                                           long blockSize, long maxCount) {
    long [] arr = new long[blockOffsets.size()];
    int idx = 0;
    for (Long offset : blockOffsets) {
      arr[idx++] = offset;
    }
    return aggregate(arr, blockSize, maxCount);
  }

  public static LinkedList<Range> aggregate(long [] blockOffsets, 
                                           long blockSize, long maxCont) {
    double [] score = new double[blockOffsets.length];
    int [] prev = new int[blockOffsets.length];
    if (!PReadDelayEstimator.isInitialized()) {
      throw new IllegalStateException("PReadDelayEstimator not initialized");
    }
    double oneBlockDelay = PReadDelayEstimator.estimateDelay(blockSize);
    double curDelay = 0;
    score[0] = oneBlockDelay;
    prev[0] = 0;
    long size = 0, len = 0;
    int i, j, k;
    for (i = 1 ; i < blockOffsets.length; ++i) {
      score[i] = score[i-1] + oneBlockDelay;
      prev[i] = i;
      for (j = 1; j <= i; ++j) {
        k = i - j;
        //System.out.println(i + ", " + k);
        len = blockOffsets[i] - blockOffsets[k] + 1;
        size = blockSize * len;
        if (len > maxCont || PReadDelayEstimator.isTooLarge(size)) 
          break;

        curDelay = PReadDelayEstimator.estimateDelay(size);
        if (k > 0)
          curDelay += score[k - 1];
        if (curDelay < score[i]) {
          score[i] = curDelay;
          prev[i] = k;
        }
      }
    }

    LinkedList<Range> ret = new LinkedList<Range>();
    i = blockOffsets.length - 1;
    while (i >= 0) {
      ret.add(0, new Range(blockOffsets[prev[i]], blockOffsets[i]));
      i = prev[i] - 1;
    }
    return ret;
  }

  public static void main(String argv[]) {
    long [] blockOffsets = {20, 21, 25, 27, 28, 34, 91, 501, 505, 506, 508, 1000};
    PReadDelayEstimator.init("1024,10;2048,11;4096,12;8192,15;16384,20;32768,30;65536,50;131072,90");
    LinkedList<Range> ret = aggregate(blockOffsets, 512, 30);
    System.out.println(ret.toString());
  }

}
