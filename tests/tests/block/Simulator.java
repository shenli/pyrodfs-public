import java.util.Random;
import java.util.LinkedList;

import java.io.IOException;

public class Simulator {

  private static final String profile = 
    "1024,10;2048,11;4096,12;8192,15;16384,20;32768,30;65536,50;131072,90";

  public static LinkedList<Long> ranges2blocks(LinkedList<Range> ranges,
                                      int blockSize, int rowSize) {
    LinkedList<Long> ret = new LinkedList<Long>();
    long startOffset = 0, endOffset = 0;
    for (Range range: ranges) {
      startOffset = range.getStart() * rowSize / blockSize;
      endOffset = range.getEnd() * rowSize / blockSize;
      for (long offset = startOffset; offset <= endOffset; ++offset) {
        if (ret.size() <= 0 || offset != ret.get(ret.size() - 1)) {
          ret.add(new Long(offset));
        }
      }
    }
    return ret;
  }


  public static Rectangle getRandRect(double xx, double yy, 
                                      GeoContext gc, long seed) {
    Random rand = new Random();
    rand.setSeed(seed);
    double ax = gc.getMaxX() * rand.nextDouble();
    double ay = gc.getMaxY() * rand.nextDouble();

    double angle = 2 * Math.PI * rand.nextDouble();
    Rectangle rect = new Rectangle(ax, ay, xx, yy);
    return rect.rotate(angle);
  }


  public static double testDelay(LinkedList<Range> aggBlocks,
                                 int blockSize, boolean isAgg)
  throws IOException {
    double totalDelay = 0;
    double oneBlockDelay = PReadDelayEstimator.estimateDelay(blockSize);
    for (Range range : aggBlocks) {
      if (isAgg) {
        long size = (range.getEnd() - range.getStart() + 1) * blockSize;
        totalDelay += PReadDelayEstimator.estimateDelay(size);
      } else {
        totalDelay += (range.getEnd() - range.getStart() + 1) * oneBlockDelay;
      }
    }

    return totalDelay;
  }

  public static void main(String argv[]) {
    double maxX = 50000L;
    double maxY = 50000L;

    // 5000 5000 25 20 10 4096 512 4000 True

    double setXX = Double.parseDouble(argv[0]);
    double setYY = Double.parseDouble(argv[1]);
    long maxResolution = Long.parseLong(argv[2]);
    long reqResolution = Long.parseLong(argv[3]);
    int testNum = Integer.parseInt(argv[4]);
    int blockSize = Integer.parseInt(argv[5]);
    int rowSize = Integer.parseInt(argv[6]);
    long maxCount = Long.parseLong(argv[7]);
    boolean isAgg = Boolean.parseBoolean(argv[8]);

    double ax, ay;
    try {
      double tileXLen = maxX / (1L << maxResolution);
      double tileYLen = maxY / (1L << maxResolution);

      GeoContext gc = new GeoContext(maxResolution, maxX, maxY);
      GeoEncoding mge = new MooreGeoEncoding(gc);

      GeoRequestParser mgrp = new BraidQuadTreeGeoRequestParser(mge);
      
      double [] delays = new double[testNum];
      PReadDelayEstimator.init(profile);
      for (int i = 0 ; i < testNum; ++i) {
        System.out.println("Test Num: " + i);
        // TODO: make sure it allows customized resolution
        RectangleGeoRequest gr = 
          new RectangleGeoRequest(getRandRect(setXX, setYY, gc, i),
          reqResolution);
        System.out.println("11111");
        LinkedList<Range> ranges = mgrp.getScanRanges(gr);
        System.out.println("22222");
        LinkedList<Long> blocks = ranges2blocks(ranges, blockSize, rowSize);
        System.out.println("33333 " + blocks.size());
        LinkedList<Range> aggBlocks =  
          BlockAggregator.aggregate(blocks, blockSize, maxCount);
        System.out.println("44444");
        delays[i] = testDelay(aggBlocks, blockSize, isAgg);
      }

      double meanDelay = 0;
      for (double delay : delays) {
        meanDelay += delay;
      }
      meanDelay = meanDelay / testNum;
      System.out.println(meanDelay);
      
    } catch (Exception ex) {
      System.out.println(ex.getMessage());
      ex.printStackTrace();
    }
  }
}
